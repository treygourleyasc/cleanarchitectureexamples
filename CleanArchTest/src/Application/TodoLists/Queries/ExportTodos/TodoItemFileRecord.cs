﻿using CleanArchTest.Application.Common.Mappings;
using CleanArchTest.Domain.Entities;

namespace CleanArchTest.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
