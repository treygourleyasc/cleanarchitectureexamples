﻿using CleanArchTest.Domain.Common;
using System.Threading.Tasks;

namespace CleanArchTest.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
