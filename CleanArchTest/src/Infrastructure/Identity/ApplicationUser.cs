﻿using Microsoft.AspNetCore.Identity;

namespace CleanArchTest.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
