﻿using CleanArchTest.Domain.Common;
using CleanArchTest.Domain.Entities;

namespace CleanArchTest.Domain.Events
{
    public class TodoItemCompletedEvent : DomainEvent
    {
        public TodoItemCompletedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
