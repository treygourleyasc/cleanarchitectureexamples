﻿using CleanArchTest.Domain.Common;
using CleanArchTest.Domain.Entities;

namespace CleanArchTest.Domain.Events
{
    public class TodoItemCreatedEvent : DomainEvent
    {
        public TodoItemCreatedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
